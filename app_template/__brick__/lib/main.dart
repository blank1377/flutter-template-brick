import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:core/app/ps_material_app.dart';
import 'package:core/translation/app_localization.dart';
import 'package:core/translation/app_localization_service.dart';
import 'package:{{project_path}}/modules/onboarding/ui/landing_page.dart';
import 'package:{{project_path}}/base/locale/app_locale.dart';
import 'package:navigation_manager/navigation/navigation_manager.dart';
import 'app_module.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AppModule.registerDependencies('dev');
  runApp(const {{project_name}}App());
}

class {{project_name}}App extends StatefulWidget {
  const {{project_name}}App({Key? key}) : super(key: key);

  @override
  State<{{project_name}}App> createState() => _{{project_name}}AppState();
}

class _{{project_name}}AppState extends State<{{project_name}}App> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    AppLocalizations.packages = AppLocale.packages;
    AppLocalizations.localeList = AppLocale.localeList;

    return PSMaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      onGenerateRoute: NavigationManager.getRoute,
      supportedLocales: AppLocale.supportedLocales,
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      localeResolutionCallback: (locale, supportedLocales) {
        // Check if the current device locale is supported
        return _getLocale(locale: locale!, supportedLocales: supportedLocales);
      },
      home: const LandingPage(),
      builder: ((context, child) {
        AppLocalizationService.context = context;
        return child ?? Container();
      }),
    );
  }

  Locale _getLocale({required Locale locale, required Iterable<Locale> supportedLocales}) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return supportedLocale;
      }
    }
    return supportedLocales.first;
  }
}