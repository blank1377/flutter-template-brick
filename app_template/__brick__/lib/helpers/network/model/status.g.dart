// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'status.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Status _$StatusFromJson(Map<String, dynamic> json) => Status(
      code: json['code'] as String?,
      header: json['header'] as String?,
      description: json['description'] as String?,
      errorCode: json['errorCode'] as String?,
    );

Map<String, dynamic> _$StatusToJson(Status instance) => <String, dynamic>{
      'code': instance.code,
      'header': instance.header,
      'description': instance.description,
      'errorCode': instance.errorCode,
    };
