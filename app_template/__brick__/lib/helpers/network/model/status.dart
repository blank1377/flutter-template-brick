import 'package:json_annotation/json_annotation.dart';
import 'package:{{project_path}}/base/repo/api_responses.dart';
import 'package:{{project_path}}/helpers/constants/app_strings.dart';

part 'status.g.dart';

@JsonSerializable()
class Status {
  String? code;
  String? header;
  String? description;
  String? errorCode;

  Status({this.code, this.header, this.description, this.errorCode});

  factory Status.fromJson(Map<String, dynamic> json) => _$StatusFromJson(json);

  Map<String, dynamic> toJson() => _$StatusToJson(this);
}

extension UnwrapStatus on Status? {
  String descriptionString() {
    return this?.description ?? AppStrings.emptyString;
  }

  Status statusInfo() {
    return this ??
        Status(
            code: ApiResponses.ERROR_CODE_1899,
            description: AppStrings.emptyString,
            header: AppStrings.emptyString);
  }
}
