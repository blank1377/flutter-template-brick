
import 'package:core/ioc/di_container.dart';
import 'package:core/module/module.dart';
import 'package:network_manager/client/i_service.dart';
import 'package:{{project_path}}/helpers/constants/service_constants/rest_service_constants.dart';
import 'package:{{project_path}}/helpers/constants/service_constants/graphql_service_constants.dart';
import 'package:{{project_path}}/helpers/constants/service_constants/file_upload_service_constants.dart';
import 'package:{{project_path}}/base/repo/services/rest_services.dart';
import 'package:{{project_path}}/base/repo/services/graphql_services.dart';
import 'package:{{project_path}}/base/repo/services/file_upload_services.dart';

class NetworkModule implements Module {
  @override
  void registerServices() {
    // Register Rest Services
    DIContainer.container.registerFactory<IService>(
      (container) => TestRestGetDataService(),
      name: RestConstants.testRestGetData,
    );

    // Register Graphql Services
    DIContainer.container.registerFactory<IService>(
      (container) => TestGraphqlQueryDataService(),
      name: GraphQLConstants.testGraphqlQueryData,
    );

    // Register File Upload Services
    DIContainer.container.registerFactory<IService>(
      (container) => TestFileUploadService(),
      name: FileUploadConstants.testUploadFile,
    );
  }
}