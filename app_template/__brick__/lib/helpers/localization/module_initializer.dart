
import 'package:core/ioc/di_container.dart';
import 'package:core/module/module.dart';
import 'package:core/translation/app_localization_service.dart';
import 'package:core/translation/i_app_localization_service.dart';

class LocalizationModule implements Module {
  @override
  void registerServices() {
    DIContainer.container.registerSingleton<IAppLocalizationService>((container) => AppLocalizationService());
  }
}