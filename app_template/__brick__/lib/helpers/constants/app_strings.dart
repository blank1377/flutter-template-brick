class AppStrings {
  AppStrings._();

  static const String correlationIDNotUsed =
      "No Correlation ID used for Rest API calls";
  static const String emptyString = "";
  static const String commaWithSpace = ", ";
  static const String comma = ",";
  static const String fullStop = ".";
  static const String questionMark = "?";
  static const String equalsTo = "=";
  static const String threeDots = "...";
  static const String divide = "/";
  static const String hashtag = "#";
  static const String colorPrefix = "0xff";
  static const String emptySpaceString = " ";
  static const String appName = "Point X";
  static const String errorBody = "Error body";
  static const String errorTitle = "Error title";
  static const String landing = "landing";
  static const String pointSlipHostName = "slip";
  static const String scan = "scan";
  static const String myPoint = "my_point";
  static const String myCoupon = "my_coupon";
  static const String tabItemsIsRequired = "tabItems is required";
  static const String myPointx = "My Point X";
  static const String uploadImage = "uploadImage";
  static const String imageUrl = "imageUrl";
  static const String hi = "Hi";
  static const String informationNotAvailable = "Information not available";
  static const String pleaseTryAgainLater = "please try again later";
  static const String pointLabel = "P";
  static const String pts = "PTS";
  static const String exclamationPoint = "!";
  static const String colon = ":";
  static const String nextLine = "\n";
  static const String plusSymbol = "+";
  static const String errorTopBar = "Point X";
  static const String hundredLeft = '100 Left';
  static const String unKnownError = 'Unknown Error';
  static const String back = "Back";
  static const String inactiveStatus = "Inactive";
  static const String guestUser = "GUEST_USER";
  static const String data = "data";
  static const String dataCapital = "Data";
  static const String code = "code";
  static const String refreshTokenAPIFailed = "refreshTokenAPIFailed";
  static const String verifyPinToRefreshTokens = "verifyPinToRefreshTokens";
  static const String transferLimitationErrorCode = '3001';
  static const String timeOutError = "TimeOutError";

  /// Keys
  static const String landingListView = "landingListView";
  static const String pointConsolidationCard = "point_consolidation_card";

  ///SectionCode
  static const String sectionCodeScanToPay = "001";
  static const String mainNavigator = "Main Navigator";

  static const String unsupportedType = "Unsupported type";

  //PointHistory
  static const String pointTransaction = "Point Transaction";
  static const String thisMonth = "This Month";

  /// Progress indicator
  static const String progressIndicatorExceptionMessage =
      "Progress value must be a double between 0.0 and 1.0";

  static const String asteriskSymbol = "*";

  static const String p_htmlStyle = "p";
  static const String b_htmlStyle = "b";
  static const String a_htmlStyle = "a";
  static const String h3_htmlStyle = "h3";
  static const String sup_htmlStyle = "sup";

  /// CountDown Timer
  static const String countDownTimerExceptionMessage =
      "CountDown Timer either use Box Background Gradient or Color";

  //Digits
  static const String ONE = '1';
  static const String TWO = '2';
  static const String THREE = '3';
  static const String FOUR = '4';
  static const String FIVE = '5';
  static const String SIX = '6';
  static const String SEVEN = '7';
  static const String EIGHT = '8';
  static const String NINE = '9';
  static const String TEN = '10';
  static const String ZERO = '0';
  static const String ZERO_ONE = '001';
  static const String ZERO_TWO = '002';
  static const String ZERO_THREE = '003';
  static const String ZERO_FOUR = '004';
  static const String ZERO_FIVE = '005';
  static const String ZERO_SIX = '006';
  static const String ZERO_SEVEN = '007';
  static const String ZERO_EIGHT = '008';
  static const String ZERO_NINE = '009';
  static const String ZERO_TEN = '010';
  static const String ZERO_ELEVEN = '011';
  static const String ZERO_TWELVE = '012';
  static const String ZERO_THIRTEEN = '013';
  static const String ZERO_FOURTEEN = '014';
  static const String THOUSAND = '1000';
  static const String ZERO_POINT_ZERO_ZERO = '0.00';

  ///brackets
  static const String roundBracketOpen = '(';
  static const String squareBracketOpen = '[';
  static const String roundBracketClose = ')';
  static const String squareBracketClose = ']';

  static const String key = "key";
  static const String value = "value";
  static const String enLangCode = "en";
  static const String thLangCode = "th";
  static const String usCountryCode = "US";
  static const String thCountryCode = "TH";
  static const String ERROR_CODE_1790 = "1790";
  static const String ERROR_CODE_1791 = "1791";
  static const String ERROR_CODE_1792 = "1792";
  static const String ERROR_CODE_1793 = "1793";
  static const String ERROR_CODE_1899 = "1899";
  static const String ERROR_CODE_2899 = "2899";
  static const String ERROR_CODE_3899 = "3899";
  static const String ERROR_CODE_3001 = "3001";
  static const String ERROR_CODE_INTERNAL_SERVER_ERROR =
      "Internal Server Error";
  static const String ERROR_CODE_404 = "404";
  static const String ERROR_CODE_400 = "400";
  static const String ERROR_CODE_1999 = "1999";
  static const String ERROR_CODE_1799 = "1799";
  static const String SUCCESS_CODE_1000 = "1000";
  static const String SUCCESS_CODE_1500 = "1500";
  static const String ERROR_CODE_0003 = "0003";
  static const String STATUS_CODE_3034 = "3034";

  /// Localizations
  static const String appPackage = 'appPackage';
  static const String EN = 'en';
  static const String TH = 'th';
}
