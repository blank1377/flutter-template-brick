class AppLocalizationKeys {
  AppLocalizationKeys._();

  static const String home = 'home';
  static const String landingHeader = 'landingHeader';
  static const String switchLocale = 'switchLocale';
  static const String bottomSheet = 'bottomSheet';
  static const String bottomSheetText = 'bottomSheetText';
  static const String dialog = 'dialog';
  static const String getData = 'getData';
  static const String close = 'close';
  static const String title = 'title';
  static const String description = 'description';
  static const String yes = 'yes';
  static const String no = 'no';
}