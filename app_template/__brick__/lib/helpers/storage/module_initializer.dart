
import 'package:core/ioc/di_container.dart';
import 'package:core/module/module.dart';
import 'package:core/storage/i_storage_service.dart';
import 'package:core/storage/secure_storage/secure_storage_service.dart';

class StorageModule implements Module {
  @override
  void registerServices() {
    final secureStorageService = SecureStorageService();
    DIContainer.container.registerSingleton<IStorageService>((container) => secureStorageService);
  }
}