import 'package:core/module/module.dart';
import 'package:navigation_manager/navigation/i_navigation_handler.dart';
import 'package:navigation_manager/navigation/navigation_manager.dart';
import 'package:{{project_path}}/base/route/route_manager.dart';
import 'package:{{project_path}}/helpers/constants/app_identifiers.dart';

class RouteModule implements Module {
  @override
  void registerServices() {
    NavigationManager(DefaultNavigationHandler());
    NavigationManager.registerRouteManager(
      AppIdentifiers.route,
      RouteManager(),
    );
  }
}