import 'package:flutter/material.dart';

abstract class RouteSwitcher {
  Widget switcher({
    ViewSwitcherData? viewSwitcherData, 
    BottomSheetSwitcherData? bottomSheetSwitcherData, 
    DialogSwitcherData? dialogSwitcherData, 
  });

  static dynamic extractData(dynamic data) {
    if (data != null) {
      data = data!;
    }
    switch(data.runtimeType) {
      case ViewSwitcherData:
        return data as ViewSwitcherData;
      case BottomSheetSwitcherData:
        return data as BottomSheetSwitcherData;
      case DialogSwitcherData:
        return data as DialogSwitcherData;
      default:
        throw Exception('Wrong data type');
    }
  }
}

class ViewSwitcherData {
  RouteSettings settings;

  ViewSwitcherData({
    required this.settings
  });
}

class BottomSheetSwitcherData {
  String bottomSheetName;
  dynamic arguments;

  BottomSheetSwitcherData({
    required this.bottomSheetName,
    required this.arguments,
  });
}

class DialogSwitcherData {
  String dialogName;
  dynamic arguments;

  DialogSwitcherData({
    required this.dialogName,
    required this.arguments,
  });
}