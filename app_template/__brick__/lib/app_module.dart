import 'package:core/analytics/default_analytics_handler.dart';
import 'package:core/analytics/i_analytics.dart';
import 'package:core/storage/i_storage_service.dart';
import 'package:core/ioc/di_container.dart';
import 'package:network_manager/network_manager.dart';
import 'package:task_manager/task_manager_module.dart';
import 'package:{{project_path}}/base/repo/app_repository.dart';
import 'package:{{project_path}}/helpers/storage/module_initializer.dart';
import 'package:{{project_path}}/helpers/network/module_initializer.dart';
import 'package:{{project_path}}/helpers/route/module_initializer.dart';
import 'package:{{project_path}}/helpers/localization/module_initializer.dart';

class AppModule {
  static Future<void> registerDependencies(String buildFlavor) async {
    NetworkModule().registerServices();
    StorageModule().registerServices();
    RouteModule().registerServices();
    LocalizationModule().registerServices();
    
    final storageService = DIContainer.container.resolve<IStorageService>();

    DIContainer.container.registerSingleton<IAnalytics>((container) => DefaultAnalyticsHandler());

    TaskManagerModule.registerDependencies(
      secureStorageService: storageService,
    );

    final appRepository = AppRepository();
    DIContainer.container.registerSingleton<AppRepository>((container) => appRepository);

    await NetworkManager.registerDependencies(
      secureStorage: storageService,
      accessTokenKey: 'accessToken',
      environment: buildFlavor
    );
  }
}