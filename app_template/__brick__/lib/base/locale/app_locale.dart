import 'package:flutter/material.dart';
import 'package:core/ioc/di_container.dart';
import 'package:core/translation/i_app_localization_service.dart';
import 'package:{{project_path}}/helpers/constants/app_strings.dart';

// This module using for PSMaterialApp
class AppLocale {
  AppLocale._();

  static const Map<String, String> packages = {AppStrings.appPackage: AppStrings.emptyString};
  static const List<String> localeList = [AppStrings.EN, AppStrings.TH];
  static const List<Locale> supportedLocales = [Locale(AppStrings.EN), Locale(AppStrings.TH)];

  static String getLocalizationText(
    String pacakage,
    String name,
  ) {
    var localization = DIContainer.container<IAppLocalizationService>();
    return localization.getValue('$pacakage:$name');
  }
}