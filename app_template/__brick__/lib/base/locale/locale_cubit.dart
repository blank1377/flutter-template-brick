import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:{{project_path}}/base/locale/app_locale.dart';

// This module using for non-PSMaterialApp, by inject cubit's locale into app
class LocaleCubit extends Cubit<Locale> {
  LocaleCubit() : super(AppLocale.supportedLocales.first);

  void changeLaguage(context, {required Locale locale}) async {
    emit(locale);
  }
}
