class GraphQLQuery {
  static String testGraphqlQueryDataQuery() {
    return '''
      query {
        testGraphqlQueryDataQuery() {
          data {
            status {
              code
              header
              description
            }
            testData {
              name
              age
              point
              alive
            }
          }
        }
      }
    ''';
  }

  static String createPostDataMutation() {
    return '''
      mutation (
        \$input: CreatePostInput!
      ) {
        createPost(input: \$input) {
          id
          title
          body
        }
      }
    ''';
  }
}
