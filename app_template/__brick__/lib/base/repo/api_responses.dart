import 'package:{{project_path}}/helpers/constants/app_strings.dart';

class ApiResponses {
  static const String ERROR_CODE_1790 = AppStrings.ERROR_CODE_1790;
  static const String ERROR_CODE_1791 = AppStrings.ERROR_CODE_1791;
  static const String ERROR_CODE_1792 = AppStrings.ERROR_CODE_1792;
  static const String ERROR_CODE_1793 = AppStrings.ERROR_CODE_1793;
  static const String ERROR_CODE_1899 = AppStrings.ERROR_CODE_1899;
  static const String ERROR_CODE_2899 = AppStrings.ERROR_CODE_2899;
  static const String ERROR_CODE_3899 = AppStrings.ERROR_CODE_3899;
  static const String ERROR_CODE_3001 = AppStrings.ERROR_CODE_3001;
  static const String ERROR_CODE_INTERNAL_SERVER_ERROR = AppStrings.ERROR_CODE_INTERNAL_SERVER_ERROR;
  static const String ERROR_CODE_404 = AppStrings.ERROR_CODE_404;
  static const String ERROR_CODE_400 = AppStrings.ERROR_CODE_400;
  static const String ERROR_CODE_1999 = AppStrings.ERROR_CODE_1999;
  static const String SUCCESS_CODE_1000 = AppStrings.SUCCESS_CODE_1000;
  static const String SUCCESS_CODE_1500 = AppStrings.SUCCESS_CODE_1500;
  static const String SUCCESS_CODE_1799 = AppStrings.ERROR_CODE_1799;
  static const String STATUS_CODE_3034 = AppStrings.STATUS_CODE_3034;
}