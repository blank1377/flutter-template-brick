import 'package:json_annotation/json_annotation.dart';
import 'package:network_manager/model/response/base_data_model.dart';
import 'package:{{project_path}}/helpers/network/model/status.dart';

part 'test_upload_file_data_model.g.dart';

@JsonSerializable()
class TestUploadFileDataModel extends BaseDataModel {
  Data? data;

  TestUploadFileDataModel({this.data});

  @override
  void fromJsonToModel(Map<String, dynamic> value) {
    TestUploadFileDataModel fromJson = _$TestUploadFileDataModelFromJson(value);
    data = fromJson.data;
  }

  factory TestUploadFileDataModel.fromJson(Map<String, dynamic> json) => _$TestUploadFileDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$TestUploadFileDataModelToJson(this);
}

@JsonSerializable()
class Data {
  TestData? testData;
  Status? status;

  Data({this.testData, this.status});

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);

  Map<String, dynamic> toJson() => _$DataToJson(this);
}

@JsonSerializable()
class TestData {
  String? name;
  int? age;
  double? point;
  bool? alive;

  TestData({this.name, this.age, this.point, this.alive});

  factory TestData.fromJson(Map<String, dynamic> json) => _$TestDataFromJson(json);

  Map<String, dynamic> toJson() => _$TestDataToJson(this);
}