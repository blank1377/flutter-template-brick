// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'test_graphql_query_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TestGraphqlQueryDataModel _$TestGraphqlQueryDataModelFromJson(
        Map<String, dynamic> json) =>
    TestGraphqlQueryDataModel(
      data: json['data'] == null
          ? null
          : Data.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TestGraphqlQueryDataModelToJson(
        TestGraphqlQueryDataModel instance) =>
    <String, dynamic>{
      'data': instance.data,
    };

Data _$DataFromJson(Map<String, dynamic> json) => Data(
      testData: json['testData'] == null
          ? null
          : TestData.fromJson(json['testData'] as Map<String, dynamic>),
      status: json['status'] == null
          ? null
          : Status.fromJson(json['status'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'testData': instance.testData,
      'status': instance.status,
    };

TestData _$TestDataFromJson(Map<String, dynamic> json) => TestData(
      name: json['name'] as String?,
      age: json['age'] as int?,
      point: (json['point'] as num?)?.toDouble(),
      alive: json['alive'] as bool?,
    );

Map<String, dynamic> _$TestDataToJson(TestData instance) => <String, dynamic>{
      'name': instance.name,
      'age': instance.age,
      'point': instance.point,
      'alive': instance.alive,
    };
