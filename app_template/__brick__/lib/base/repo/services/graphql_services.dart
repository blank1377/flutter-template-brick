import 'package:core/task/task_params.dart';
import 'package:network_manager/client/i_service.dart';
import 'package:network_manager/model/requests/base_request.dart';
import 'package:network_manager/model/requests/graph_ql/graphql_request.dart';
import 'package:network_manager/model/requests/rest/file_upload_request.dart';
import 'package:network_manager/model/requests/rest/rest_request.dart';
import 'package:{{project_path}}/base/repo/graphql_queries.dart';
import 'package:{{project_path}}/base/repo/data_model/test_graphql_query_data_model.dart';
import 'package:{{project_path}}/helpers/constants/service_constants/graphql_service_constants.dart';

class TestGraphqlQueryDataService implements BaseService {
  @override
  GraphQLRequest getGraphQLRequest(TaskParams? params, {Map<String, dynamic>? paramsInMap}) {
    final graphqlDataModel = TestGraphqlQueryDataModel();
    final request = GraphQLQuery.testGraphqlQueryDataQuery();
    return GraphQLRequest(
      type: RequestType.query,
      name: GraphQLConstants.testGraphqlQueryData,
      data: graphqlDataModel,
      request: request,
    );
  }

  /// No usage
  @override
  RestRequest getRestRequest(TaskParams? params, {Map<String, dynamic>? paramsInMap}) {
    throw UnimplementedError();
  }

  @override
  FileUploadRequest getFileUploadRequest(TaskParams? params, {Map<String, dynamic>? paramsInMap}) {
    throw UnimplementedError();
  }
}
