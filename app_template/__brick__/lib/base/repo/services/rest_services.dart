import 'package:core/task/task_params.dart';
import 'package:network_manager/client/i_service.dart';
import 'package:network_manager/model/requests/base_request.dart';
import 'package:network_manager/model/requests/graph_ql/graphql_request.dart';
import 'package:network_manager/model/requests/rest/file_upload_request.dart';
import 'package:network_manager/model/requests/rest/rest_request.dart';
import 'package:{{project_path}}/base/repo/data_model/test_rest_get_data_model.dart';
import 'package:{{project_path}}/helpers/constants/service_constants/rest_service_constants.dart';

class TestRestGetDataService implements BaseService {
  @override
  RestRequest getRestRequest(TaskParams? params, {Map<String, dynamic>? paramsInMap}) {
    final restDataModel = TestRestGetDataModel();
    return RestRequest(
      type: RequestType.get,
      name: RestConstants.testRestGetData,
      data: restDataModel,
    );
  }

  /// No usage
  @override
  GraphQLRequest getGraphQLRequest(TaskParams? params, {Map<String, dynamic>? paramsInMap}) {
    throw UnimplementedError();
  }

  @override
  FileUploadRequest getFileUploadRequest(TaskParams? params, {Map<String, dynamic>? paramsInMap}) {
    throw UnimplementedError();
  }
}
