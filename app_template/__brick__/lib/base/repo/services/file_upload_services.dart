import 'package:core/task/task_params.dart';
import 'package:network_manager/client/i_service.dart';
import 'package:network_manager/model/requests/graph_ql/graphql_request.dart';
import 'package:network_manager/model/requests/rest/file_upload_request.dart';
import 'package:network_manager/model/requests/rest/rest_request.dart';
import 'package:{{project_path}}/base/repo/data_model/test_upload_file_data_model.dart';
import 'package:{{project_path}}/helpers/constants/service_constants/file_upload_service_constants.dart';

class TestFileUploadService implements BaseService {
  @override
  FileUploadRequest getFileUploadRequest(TaskParams? params, {Map<String, dynamic>? paramsInMap}) {
    final fileUploadDataModel = TestUploadFileDataModel();
    return FileUploadRequest(
      name: FileUploadConstants.testUploadFile,
      additionalHeaders: paramsInMap?['headers'],
      body: paramsInMap?['body'],
      file: paramsInMap?['file'],
      formDataFileKey: paramsInMap?['fileKey'],
      data: fileUploadDataModel,
    );
  }

  /// No usage
  @override
  GraphQLRequest getGraphQLRequest(TaskParams? params, {Map<String, dynamic>? paramsInMap}) {
    throw UnimplementedError();
  }

  @override
  RestRequest getRestRequest(TaskParams? params, {Map<String, dynamic>? paramsInMap}) {
    throw UnimplementedError();
  }
}
