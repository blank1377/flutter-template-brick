import 'dart:io';
import 'package:core/ioc/di_container.dart';
import 'package:task_manager/task_manager_impl/task_manager_impl.dart';
import 'package:task_manager/task.dart';
import 'package:{{project_path}}/helpers/constants/service_constants/rest_service_constants.dart';
import 'package:{{project_path}}/helpers/constants/service_constants/graphql_service_constants.dart';
import 'package:{{project_path}}/helpers/constants/service_constants/file_upload_service_constants.dart';

import 'data_model/test_rest_get_data_model.dart';
import 'data_model/test_graphql_query_data_model.dart';
import 'data_model/test_upload_file_data_model.dart';

class AppRepository {
  final taskManager = DIContainer.container.resolve<TaskManager>();

  Future<TestRestGetDataModel> testRestGetData() async {
    final result = await taskManager.waitForExecute<TestRestGetDataModel>(
      Task(
        apiIdentifier: RestConstants.testRestGetData,
        taskType: TaskType.DATA_OPERATION,
        subType: TaskSubType.REST,
      ),
    );
    return result;
  }

  Future<TestGraphqlQueryDataModel> testGraphqlQueryData() async {
    final result = await taskManager.waitForExecute<TestGraphqlQueryDataModel>(
      Task(
        apiIdentifier: GraphQLConstants.testGraphqlQueryData,
        taskType: TaskType.DATA_OPERATION,
        subType: TaskSubType.GRAPHQL,
      ),
    );
    return result;
  }

  Future<TestUploadFileDataModel> testUploadFile({
    required Map<String, dynamic> additionalHeaders,
    required Map<String, String> metaData,
    required File file,
    String? fileKey
  }) async {
    final result = await taskManager.waitForExecute<TestUploadFileDataModel>(
      Task(
        apiIdentifier: FileUploadConstants.testUploadFile,
        paramsInMap: {
          'headers': additionalHeaders,
          'body': metaData,
          'file': file,
          'fileKey': fileKey,
        },
        taskType: TaskType.DATA_OPERATION,
        subType: TaskSubType.REST_FILE_UPLOAD,
      ),
    );
    return result;
  }
}