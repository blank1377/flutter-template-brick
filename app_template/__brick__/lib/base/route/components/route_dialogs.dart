import 'package:flutter/material.dart';
import 'package:{{project_path}}/modules/home/ui/widgets/base_dialog.dart';
import 'package:{{project_path}}/helpers/route/route_helpers.dart';

class RouteDialogs extends RouteSwitcher {
  // Define your bottom sheet routes name here.
  static const String BASE_DIALOG = 'BASE_DIALOG';

  @override
  Widget switcher({
    ViewSwitcherData? viewSwitcherData,
    BottomSheetSwitcherData? bottomSheetSwitcherData, 
    DialogSwitcherData? dialogSwitcherData, 
  }){
    dialogSwitcherData = RouteSwitcher
      .extractData(dialogSwitcherData) as DialogSwitcherData;
    var dialogName = dialogSwitcherData.dialogName;
    var arguments = dialogSwitcherData.arguments;
    switch (dialogName) {
      // Define which dialog you want to return when route is called.
      case RouteDialogs.BASE_DIALOG:
        return const BaseDialog();
      default:
        throw Exception('Route $dialogName not found');
    }
  }
}