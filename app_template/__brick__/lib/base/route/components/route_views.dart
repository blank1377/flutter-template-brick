import 'package:flutter/material.dart';
import 'package:{{project_path}}/modules/home/ui/home_screen.dart';
import 'package:{{project_path}}/helpers/route/route_helpers.dart';

class RouteViews extends RouteSwitcher {
  // Define your view routes name here.
  static const String HOME = 'HOME';

  @override
  Widget switcher({
    ViewSwitcherData? viewSwitcherData,
    BottomSheetSwitcherData? bottomSheetSwitcherData, 
    DialogSwitcherData? dialogSwitcherData, 
  }){
    viewSwitcherData = RouteSwitcher
      .extractData(viewSwitcherData) as ViewSwitcherData;
    var settings = viewSwitcherData.settings;
    switch (settings.name) {
      // Define which view you want to return when route is called.
      case RouteViews.HOME:
        return const HomeScreen();
      default:
        throw Exception('Route ${settings.name} not found');
    }
  }
}