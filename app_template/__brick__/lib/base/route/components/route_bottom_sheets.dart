import 'package:flutter/material.dart';
import 'package:{{project_path}}/modules/home/ui/widgets/base_bottom_sheet.dart';
import 'package:{{project_path}}/helpers/route/route_helpers.dart';

class RouteBottomSheets extends RouteSwitcher {
  // Define your bottom sheet routes name here.
  static const String BASE_BOTTOM_SHEET = 'BASE_BOTTOM_SHEET';

  @override
  Widget switcher({
    ViewSwitcherData? viewSwitcherData,
    BottomSheetSwitcherData? bottomSheetSwitcherData, 
    DialogSwitcherData? dialogSwitcherData, 
  }){
    bottomSheetSwitcherData = RouteSwitcher
      .extractData(bottomSheetSwitcherData) as BottomSheetSwitcherData;
    var bottomSheetName = bottomSheetSwitcherData.bottomSheetName;
    var arguments = bottomSheetSwitcherData.arguments;
    switch (bottomSheetName) {
      // Define which bottom sheet you want to return when route is called.
      case RouteBottomSheets.BASE_BOTTOM_SHEET:
        return const BaseBottomSheet();
      default:
        throw Exception('Route $bottomSheetName not found');
    }
  }
}