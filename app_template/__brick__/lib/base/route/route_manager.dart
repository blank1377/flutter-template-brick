import 'package:flutter/material.dart';
import 'package:navigation_manager/navigation/i_route_manager.dart';
import 'package:{{project_path}}/base/route/components/route_views.dart';
import 'package:{{project_path}}/base/route/components/route_bottom_sheets.dart';
import 'package:{{project_path}}/base/route/components/route_dialogs.dart';
import 'package:{{project_path}}/helpers/route/route_helpers.dart';

class RouteManager extends IRouteManager {
  @override
  Widget getView(RouteSettings settings) {
    return RouteViews().switcher(
      viewSwitcherData: ViewSwitcherData(settings: settings),
    );
  }

  @override
  Widget getBottomSheet(String bottomSheetName, dynamic arguments) {
    return RouteBottomSheets().switcher(
      bottomSheetSwitcherData: BottomSheetSwitcherData(
        bottomSheetName: bottomSheetName,
        arguments: arguments,
      ),
    );
  }

  @override
  Widget getDialog(String dialogName, dynamic arguments) {
    return RouteDialogs().switcher(
      dialogSwitcherData: DialogSwitcherData(
        dialogName: dialogName,
        arguments: arguments
      ),
    );
  }

  static String getRouteName(String identifier, String name) {
    return '$identifier-$name';
  }
}
