import 'package:core/translation/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:{{project_path}}/base/route/route_manager.dart';
import 'package:{{project_path}}/base/route/components/route_views.dart';
import 'package:{{project_path}}/base/locale/app_locale.dart';
import 'package:{{project_path}}/helpers/constants/app_identifiers.dart';
import 'package:{{project_path}}/helpers/constants/app_localization_keys.dart';
import 'package:{{project_path}}/helpers/constants/app_strings.dart';
import 'package:navigation_manager/navigation/navigation_manager.dart';

class LandingPage extends StatefulWidget {
  const LandingPage({Key? key}) : super(key: key);

  @override
  State<LandingPage> createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocale.getLocalizationText(
            AppStrings.appPackage,
            AppLocalizationKeys.landingHeader,
          ),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              onPressed: () {
                NavigationManager.navigateTo(
                  RouteManager.getRouteName(
                    AppIdentifiers.route, RouteViews.HOME
                  ),
                  NavigationType.ReplaceCurrent,
                );
              },
              child: Text(
                AppLocale.getLocalizationText(
                  AppStrings.appPackage,
                  AppLocalizationKeys.home,
                ),
              ),
            ),
            ElevatedButton(
              onPressed: () async {
                Locale newLocale;
                if (AppLocalizations.of(context)?.locale.languageCode == AppStrings.EN) {
                  newLocale = const Locale(AppStrings.TH, AppStrings.emptyString);
                } else {
                  newLocale = const Locale(AppStrings.EN, AppStrings.emptyString);
                }
                AppLocalizations.of(context)?.changeLocale(newLocale);
              },
              child: Text(
                AppLocale.getLocalizationText(
                  AppStrings.appPackage,
                  AppLocalizationKeys.switchLocale,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}