import 'package:flutter/material.dart';
import 'package:navigation_manager/navigation/navigation_manager.dart';
import 'package:{{project_path}}/base/locale/app_locale.dart';
import 'package:{{project_path}}/helpers/constants/app_strings.dart';
import 'package:{{project_path}}/helpers/constants/app_localization_keys.dart';

class BaseDialog extends StatelessWidget {
  const BaseDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }
  contentBox(context){
    return Stack(
      children: <Widget>[
        Container(
          padding: const EdgeInsets.only(left: 20,top: 45 + 20, right: 20,bottom: 20),
          margin: const EdgeInsets.only(top: 45),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: const [
                BoxShadow(color: Colors.black,offset: Offset(0,10),
                    blurRadius: 10
                ),
              ]
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                AppLocale.getLocalizationText(
                  AppStrings.appPackage,
                  AppLocalizationKeys.title,
                ),
              ),
              const SizedBox(height: 15,),
              Text(
                AppLocale.getLocalizationText(
                  AppStrings.appPackage,
                  AppLocalizationKeys.description,
                ),
              ),
              const SizedBox(height: 22),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  OutlinedButton(
                    onPressed: (){
                      NavigationManager.goBack(arguments: true);
                    },
                    child: Text(
                      AppLocale.getLocalizationText(
                        AppStrings.appPackage,
                        AppLocalizationKeys.yes,
                      ),
                    ),
                  ),
                  OutlinedButton(
                    onPressed: (){
                      NavigationManager.goBack(arguments: false);
                    },
                    child: Text(
                      AppLocale.getLocalizationText(
                        AppStrings.appPackage,
                        AppLocalizationKeys.no,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}