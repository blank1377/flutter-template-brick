import 'package:flutter/material.dart';
import 'package:navigation_manager/navigation/navigation_manager.dart';
import 'package:{{project_path}}/base/locale/app_locale.dart';
import 'package:{{project_path}}/helpers/constants/app_strings.dart';
import 'package:{{project_path}}/helpers/constants/app_localization_keys.dart';

class BaseBottomSheet extends StatelessWidget {
  const BaseBottomSheet({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        children: [
          const SizedBox(height: 20),
          Center(
            child: Text(
              AppLocale.getLocalizationText(
                AppStrings.appPackage,
                AppLocalizationKeys.bottomSheetText,
              ),
            ),
          ),
          const SizedBox(height: 20),
          OutlinedButton(
            onPressed: (){
              NavigationManager.goBack();
            },
            child: Text(
              AppLocale.getLocalizationText(
                AppStrings.appPackage,
                AppLocalizationKeys.close,
              ),
            ),
          ),
        ],
      ),
    );
  }
}