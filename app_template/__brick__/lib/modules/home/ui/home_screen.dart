import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:core/ioc/di_container.dart';
import 'package:{{project_path}}/base/repo/app_repository.dart';
import 'package:{{project_path}}/base/route/components/route_bottom_sheets.dart';
import 'package:{{project_path}}/base/route/components/route_dialogs.dart';
import 'package:{{project_path}}/base/route/route_manager.dart';
import 'package:{{project_path}}/base/locale/app_locale.dart';
import 'package:{{project_path}}/helpers/constants/app_identifiers.dart';
import 'package:{{project_path}}/helpers/constants/app_strings.dart';
import 'package:{{project_path}}/helpers/constants/app_localization_keys.dart';
import 'package:navigation_manager/navigation/navigation_manager.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final appRepository = DIContainer.container.resolve<AppRepository>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocale.getLocalizationText(
            AppStrings.appPackage,
            AppLocalizationKeys.home
          ),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              onPressed: () {
                NavigationManager.navigateTo(
                  RouteManager.getRouteName(
                    AppIdentifiers.route, RouteBottomSheets.BASE_BOTTOM_SHEET
                  ),
                  NavigationType.BottomSheet,
                );
              },
              child: Text(
                AppLocale.getLocalizationText(
                  AppStrings.appPackage,
                  AppLocalizationKeys.bottomSheet
                ),
              ),
            ),
            ElevatedButton(
              onPressed: () {
                NavigationManager.navigateTo(
                  RouteManager.getRouteName(
                    AppIdentifiers.route, RouteDialogs.BASE_DIALOG
                  ),
                  NavigationType.Dialog,
                );
              },
              child: Text(
                AppLocale.getLocalizationText(
                  AppStrings.appPackage,
                  AppLocalizationKeys.dialog
                ),
              ),
            ),
            ElevatedButton(
              onPressed: _onGetData,
              child: Text(
                AppLocale.getLocalizationText(
                  AppStrings.appPackage,
                  AppLocalizationKeys.getData
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onGetData() async {
    var apiResponse = await appRepository.testRestGetData();
    inspect(apiResponse);
  }
}